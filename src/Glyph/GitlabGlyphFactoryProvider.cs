﻿using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Utilities;
using System.ComponentModel.Composition;

namespace GitlabIssuePicker.Glyph
{
    [Export(typeof(IGlyphFactoryProvider))]
    [Name("GitlabGlyph")]
    [Order(Before = "VsTextMarker")]
    [ContentType("code")]
    [TagType(typeof(GitlabTag))]
    internal sealed class GitlabGlyphFactoryProvider : IGlyphFactoryProvider
    {
        public IGlyphFactory GetGlyphFactory(IWpfTextView view, IWpfTextViewMargin margin) => new GitlabGlyphFactory();
    }
}
