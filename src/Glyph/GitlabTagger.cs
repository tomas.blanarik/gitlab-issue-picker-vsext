﻿using GitlabIssuePicker.Options;
using GitlabIssuePicker.Support;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Tagging;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace GitlabIssuePicker.Glyph
{
    internal class GitlabTagger : RegexTagger<GitlabTag>
    {
        public GitlabTagger(ITextBuffer buffer) : base(buffer)
        { }

        public override IEnumerable<ITagSpan<GitlabTag>> GetTags(NormalizedSnapshotSpanCollection spans)
        {
            var options = GeneralOptions.Instance;
            if (options.IsGlyphEnabled)
            {
                return base.GetTags(spans);
            }

            return Enumerable.Empty<ITagSpan<GitlabTag>>();
        }

        protected override GitlabTag TryCreateTagForMatch(Match match)
        {
            return new GitlabTag();
        }
    }
}
