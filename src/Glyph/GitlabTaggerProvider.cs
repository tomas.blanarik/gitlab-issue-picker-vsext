﻿using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Utilities;
using System;
using System.ComponentModel.Composition;

namespace GitlabIssuePicker.Glyph
{
    [Export(typeof(ITaggerProvider))]
    [ContentType("code")]
    [TagType(typeof(GitlabTag))]
    internal class GitlabTaggerProvider : ITaggerProvider
    {
        public ITagger<T> CreateTagger<T>(ITextBuffer buffer) where T : ITag
        {
            if (buffer == null)
            {
                throw new ArgumentNullException(nameof(buffer));
            }

            return new GitlabTagger(buffer) as ITagger<T>;
        }
    }
}
