﻿using GitlabIssuePicker.Rest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace GitlabIssuePicker.Client
{
    internal class GitlabClient
    {
        private static readonly Lazy<HttpClient> _httpClientAccessor = new Lazy<HttpClient>(() => new HttpClient());
        private const string _issueApiUrl = "api/v4/issues?iids[]={0}";
        private const string _privateHeaderKey = "PRIVATE-TOKEN";

        private readonly string _url;
        private readonly string _personalToken;

        private static HttpClient Client => _httpClientAccessor.Value;

        public GitlabClient(string url, string personalToken)
        {
            _url = url;
            _personalToken = personalToken;
        }

        public async Task<IssueModel> GetIssueAsync(int iid)
        {
            if (!string.IsNullOrEmpty(_personalToken))
            {
                var url = GetUrl(iid);
                var request = new HttpRequestMessage(HttpMethod.Get, url);
                request.Headers.Add(_privateHeaderKey, _personalToken);

                var response = await Client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    var issues = JsonConvert.DeserializeObject<List<IssueModel>>(responseString);
                    if (issues.Count == 0)
                    {
                        return null;
                    }

                    return issues[0];
                }

                return null;
            }

            return null;
        }

        private string GetUrl(int iid)
        {
            return $"{_url.TrimEnd('/')}/{string.Format(_issueApiUrl, iid)}";
        }
    }
}
