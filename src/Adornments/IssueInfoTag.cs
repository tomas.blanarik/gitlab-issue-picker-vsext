﻿using Microsoft.VisualStudio.Text.Tagging;

namespace GitlabIssuePicker.Adornments
{
    internal class IssueInfoTag : ITag
    {
        public string Key { get; set; }
    }
}
