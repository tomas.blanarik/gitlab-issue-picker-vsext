﻿using GitlabIssuePicker.Options;
using GitlabIssuePicker.Support;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Tagging;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace GitlabIssuePicker.Adornments
{
    internal sealed class IssueInfoTagger : RegexTagger<IssueInfoTag>
    {
        public IssueInfoTagger(ITextBuffer buffer) : base(buffer)
        { }

        public override IEnumerable<ITagSpan<IssueInfoTag>> GetTags(NormalizedSnapshotSpanCollection spans)
        {
            var options = GeneralOptions.Instance;
            if (options.IsAdnornmentEnabled)
            {
                return base.GetTags(spans);
            }

            return Enumerable.Empty<ITagSpan<IssueInfoTag>>();
        }

        protected override IssueInfoTag TryCreateTagForMatch(Match match)
        {
            return new IssueInfoTag
            {
                Key = match.Value.ToLower().Replace("gitlab-", "")
            };
        }
    }
}
