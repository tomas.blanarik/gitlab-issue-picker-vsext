﻿using Newtonsoft.Json;

namespace GitlabIssuePicker.Rest
{
    internal class IssueModel
    {
        [JsonProperty("iid")]
        public int Iid { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("web_url")]
        public string WebUrl { get; set; }

        [JsonProperty("author")]
        public IssueAuthor Author { get; set; }

        public override string ToString()
        {
            return $"[#{Iid}]-{Title}";
        }
    }
}
