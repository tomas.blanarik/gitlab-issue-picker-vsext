﻿using Newtonsoft.Json;

namespace GitlabIssuePicker.Rest
{
    internal class IssueAuthor
    {
        [JsonProperty("web_url")]
        public string Url { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("avatar_url")]
        public string Avatar { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }
    }
}
