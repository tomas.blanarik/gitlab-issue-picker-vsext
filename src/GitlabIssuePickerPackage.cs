﻿using GitlabIssuePicker.Options;
using Microsoft.VisualStudio.Shell;
using System;
using System.Runtime.InteropServices;
using System.Threading;
using Task = System.Threading.Tasks.Task;

namespace GitlabIssuePicker
{
    [PackageRegistration(UseManagedResourcesOnly = true, AllowsBackgroundLoading = true)]
    [Guid(PackageGuidString)]
    [ProvideOptionPage(typeof(DialogPageProvider.General), "Gitlab Issue Picker", "General", 0, 0, true)]
    public sealed class GitlabIssuePickerPackage : AsyncPackage
    {
        public const string PackageGuidString = "31a64a8a-ffd9-48d0-81c7-6385d6d468bd";

        protected override async Task InitializeAsync(CancellationToken cancellationToken, IProgress<ServiceProgressData> progress)
        {
            await base.InitializeAsync(cancellationToken, progress);
        }
    }
}
