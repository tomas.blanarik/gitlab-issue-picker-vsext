﻿using System.ComponentModel;

namespace GitlabIssuePicker.Options
{
    internal class GeneralOptions : BaseOptionModel<GeneralOptions>
    {
        [Category("Connection Settings")]
        [DisplayName("Gitlab Host Address")]
        [Description("Please provide gitlab instance url (e.g. https://gitlab.com)")]
        public string GitlabHost { get; set; } = "https://gitlab.com";

        [Category("Connection Settings")]
        [DisplayName("Gitlab Access Token")]
        [Description("Please provide your personal access token")]
        [PasswordPropertyText(true)]
        public string PersonalToken { get; set; }

        [Category("Misc")]
        [DisplayName("Issue Regexes")]
        [Description("Specify by which regexes gitlab issues will be searched in the editor")]
        public string[] SearchRegexes { get; set; } = new[] { @"gitlab-#[\d+]*" };

        [Category("Misc")]
        [DisplayName("Enable Glyph Rendering")]
        [Description("Specify whether glyph should be shown indicating gitlab issue in the left panel")]
        public bool IsGlyphEnabled { get; set; }

        [Category("Misc")]
        [DisplayName("Enable Adornment Rendering")]
        [Description("Specify whether adornment should be shown indicating gitlab issue key and title in the comment")]
        public bool IsAdnornmentEnabled { get; set; }
    }
}
