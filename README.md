# Gitlab Issue Picker 
## Visual Studio Extension
This is an open source project containing code for Visual Studio Extension 
called Gitlab Issue Picker, which will automatically pick commented 
gitlab issue keys from comments and fetch information about them.

## Contribution
Feel free to create a pull request with requested changes.

### Author
Tomas Blanarik  
Visma Labs  
Slovakia
